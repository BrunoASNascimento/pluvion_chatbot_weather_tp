def get_json(value, info_str, icon, unit):
    data = {
        "status": True,
        "value": round(value, 1),
        "unit": unit,
        "info_str": info_str,
        "icon": icon
    }

    return data


def arh(value):
     # umidade
    # tabela de acordo com o site CGESP.ORG para os estados de atencao, alerta e emergencia
    # o range ideal e de acordo com a recomendacao da OMS
    # os ranges que sobraram foram classificados em "baixa" e "alta"
    if value <= 12:
        info_str = "Estado de Emergência (Muito Baixa)"
        icon = "🥵"
    elif value > 12 and value <= 20:
        info_str = "Estado de Alerta (Muito Baixa)"
        icon = "🥵"
    elif value > 20 and value <= 30:
        info_str = "Estado de Atenção (Baixa)"
        icon = "💧"
    elif value > 30 and value <= 50:
        info_str = "Baixa"
        icon = "💧"
    elif value > 50 and value <= 80:
        info_str = "Ideal"
        icon = "💧💧"
    elif value > 80:
        info_str = "Alta"
        icon = "💧💧💧"
    unit = "%"
    data_json = get_json(value, info_str, icon, unit)

    return data_json


def pc_int(value):

    if value < 0.25:
        info_str = "sem chuva"
        icon = "☀"
    elif value >= 0.25 and value < 2.5:
        info_str = "chuva fraca"
        icon = "🌦"
    elif value >= 2.5 and value < 7.6:
        info_str = "chuva moderada"
        icon = "🌧"
    elif value >= 7.6:
        info_str = "chuva forte"
        icon = "⛈"

    unit = "mm/h"
    data_json = get_json(value, info_str, icon, unit)

    return data_json


def pc_vol(value):
    if value < 0.26:
        info_str = "sem chuva"
        icon = "☀"
    elif value >= 0.26 and value < 3:
        info_str = "pouca chuva"
        icon = "🌦"
    elif value >= 3 and value < 10:
        info_str = "chuva moderada"
        icon = "🌧"
    elif value >= 10 and value < 50:
        info_str = "bastante chuva"
        icon = "🌧"
    elif value >= 50:
        info_str = "muita chuva"
        icon = "⛈"

    unit = "mm"
    data_json = get_json(value, info_str, icon, unit)

    return data_json


def tp(value):
    if (value < 15):
        info_str = "muito frio"
        icon = "⛇"
    elif (value >= 15 and value < 20):
        info_str = "frio"
        icon = "🥶"
    elif (value >= 20 and value < 25):
        info_str = "ameno"
        icon = "🌤"
    elif (value >= 25 and value < 30):
        info_str = "calor"
        icon = "☀"
    elif (value >= 30):
        info_str = "muito calor"
        icon = "🥵"
    unit = "°C"
    data_json = get_json(value, info_str, icon, unit)

    return data_json


def ws(value):
    # Escala de Beaufort SIMPLIFICADA, e utilizando a media dos valores de vento considerados
    if value <= 0.3:
        info_str = "tranquilo"
        icon = "🌬"
    elif value > 0.3 and value <= 3.3:
        info_str = "suave"
        icon = "🌬"
    elif value > 3.3 and value <= 7.9:
        info_str = "moderado"
        icon = "🌬"
    elif value > 7.9 and value <= 10.7:
        info_str = "vivo"
        icon = "🌬"
    elif value > 10.7 and value <= 17.1:
        info_str = "forte"
        icon = "🌬"
    elif value > 17.1 and value <= 24.4:
        info_str = "muito forte"
        icon = "🌬"
    elif value > 24.4 and value <= 32.6:
        info_str = "massivo"
        icon = "🌬"
    elif value > 32.6:
        info_str = "Furacão"
        icon = "🌪"

    value = (value*3.6)
    unit = "km/h"
    data_json = get_json(value, info_str, icon, unit)

    return data_json
