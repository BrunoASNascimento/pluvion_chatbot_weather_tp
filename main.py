import requests
import json
from datetime import datetime, timedelta
from flask import jsonify
import pluvion_chatbot_scale as scale

power_idw = 2


def config_date(day_back):
    #(dateNow, dateBefore, dateOneHour, datePcIntNow) = config_date()
    dateNow = datetime.utcnow()-timedelta(days=day_back)
    dateBefore = dateNow+timedelta(days=1)
    dateOneHour = dateNow-timedelta(hours=1)
    datePcIntNow = dateNow-timedelta(minutes=5)
    return (dateNow, dateBefore, dateOneHour, datePcIntNow)


def idw(data_idw, power):
    # data_idw = [[value1,dist1],[value2,dist2]...[valuen,distn]]
    numerator = []
    denominator = []
    for data in data_idw:
        if data[1] == 0:
            return data[0]
        numerator.append(data[0]/(data[1]**power))
        denominator.append(1/data[1]**power)
    try:
        z = sum(numerator)/sum(denominator)
    except ZeroDivisionError:
        z = sum(numerator)
    return z


def getStations(lat, lon):
    url = "https://us-central1-pluvion-tech.cloudfunctions.net/pluvion_chatbot_closest_stations/"
    querystring = {"lat": lat, "lon": lon}
    response = requests.request("GET", url, params=querystring)
    data = json.loads(response.text)
    data = data['data']
    return data


def getDataWeather(station, day_back):
    (dateNow, dateBefore, dateOneHour, datePcIntNow) = config_date(day_back)
    url = "https://us-central1-pluvion-tech.cloudfunctions.net/tools_fs/get"
    value = (
        str(station) +
        dateNow.strftime('-%Y%m%d_030000') +
        dateBefore.strftime('-%Y%m%d_025959')
    )
    print(value)
    payload = {"collectionName": "msg_summary",
               "filters":
               {
                   "where": [
                       {"field": "sumCode",
                        "op": "==",
                        "value": value
                        }
                   ]
               }
               }
    headers = {
        'Content-Type': "application/json",
        'Host': "us-central1-pluvion-tech.cloudfunctions.net",
        'Accept-Encoding': "gzip, deflate",
        'Content-Length': "251",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request(
        "POST", url, data=json.dumps(payload), headers=headers)
    data = json.loads(response.text)
    #print('Data: ', data)
    try:
        data = data['data']
    except:
        data = []
    return data


def arrayDataWeather(lat, lon):
    stations = getStations(lat, lon)
    data = []
    for station in stations:
        weather = getDataWeather(station[0], 0)
        if weather != []:
            data.append([weather, station[1]])
        else:
            data.append([getDataWeather(station[0], 1), station[1]])
    # print(len(data))
    return data


def dataParserAll(lat, lon):

    data = arrayDataWeather(lat, lon)

    tp_max = []
    tp_min = []
    tp_avg = []

    for data_parter in data:
        if (data_parter[0] != []):

            haversine = data_parter[1]

            if (data_parter[0][0]['atp']['last']['atpMax'] != -30):  # Error Handling
                tp_max.append(
                    [data_parter[0][0]['atp']['last']['atpMax'], haversine]
                )
                tp_min.append(
                    [data_parter[0][0]['atp']['last']['atpMin'], haversine]
                )
                tp_avg.append(
                    [data_parter[0][0]['atp']['last']['atpAvg'], haversine]
                )

    tp = {
        "tp_max":  scale.tp(idw(tp_max, power_idw)),
        "tp_min":  scale.tp(idw(tp_min, power_idw)),
        "tp_avg":  scale.tp(idw(tp_avg, power_idw))
    }

    #print(tp, ws, arh, pc)

    data = {
        "status": True,
        "data":
        {
            "tp": tp
        }
    }
    print(data)
    return data


def pluvion_chatbot_weather_tp(request):
    json_req = request.args.to_dict(flat=False)
    # url/?lat=value&lon=value
    lat = round(float((json_req['lat'])[0]), 3)
    lon = round(float((json_req['lon'])[0]), 3)
    print(lat, lon)
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for the main request
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    try:
        data = dataParserAll(lat, lon)
    except:
        data = {"status": False}

    return (jsonify(data), 200, headers)


# dataParserAll(-23.55367, -46.65934)
